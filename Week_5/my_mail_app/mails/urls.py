from django.urls import path

from . import views

urlpatterns = [
  path('new_mail', views.new_mail, name='new_mail'),
  path('inbox', views.inbox, name='inbox'),
  path('sentbox', views.sentbox, name='sentbox'),
  path('dashboard', views.dashboard, name='dashboard')
]