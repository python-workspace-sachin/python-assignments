from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Mails
from .forms import Compose_mail
from django.contrib.auth.models import User

def new_mail(request):
  #if request.method == 'POST':
    #mail_id = request.POST['mail_id']
    #from_id = request.user.email
    # to_id = request.POST['to_id']
    # subject = request.POST['subject']
    # body = request.POST['body']
    #time_stamp = request.POST['time_stamp']
    #deleted_from_id = request.POST['deleted_from_id']
    #deleted_to_id = request.POST['deleted_to_id']

    if request.method == "GET":
        form = Compose_mail()
        return render(request,'dashboard.html',{'form':form})
    elif request.method == 'POST':
        
        form = Compose_mail(request.POST)
        if form.is_valid():
            
            form.save()
    
    messages.success(request, 'Your mail has been sent!')
    return redirect('dashboard.html')

def inbox(request):
    if request.method == 'GET':
        messages = Mails.objects.filter(to_id=request.user.email).order_by('-time_stamp')
        context = {
            'messages': messages
            }
        return render(request, "inbox.html", context)

def sentbox(request):
    if request.method == 'GET':
        messages = Mails.objects.filter(from_id=request.user.email).order_by('-time_stamp')
        context = {
            'messages': messages
            }
        return render(request, "sentbox.html", context)

def dashboard(request):
  return render(request, 'accounts/dashboard.html')
