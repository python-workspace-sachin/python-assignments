from django.db import models
from datetime import datetime

class Mails(models.Model):
    mail_id = models.AutoField(primary_key=True)
    from_id = models.CharField(max_length=200)
    to_id = models.CharField(max_length=200)
    subject = models.CharField(max_length=200)
    body = models.TextField()
    time_stamp = models.DateTimeField(default=datetime.now)
    deleted_from_id = models.BooleanField(default=False)
    deleted_to_id = models.BooleanField(default=False)
    def __str__(self):
        return mail_id