from django import forms
from .models import Mails

class Compose_mail(forms.ModelForm):

    class Meta:
        model = Mails
        fields = ('from_id','to_id', 'subject', 'body')