#Write a program to print all prime numbers between 0, 10 using list comprehension.

#Store the given range numbers
lo = 0
hi = 10

#Create an empty list which will be filled with prime numbers
prime_list = []

#Using for loops
#Iterate over the given range
for num in range(lo, hi + 1):
    #num must be greater than 1 as 0 or 1 are not prime nos.
    if num > 1:
        #Check division of num by every num
        for i in range(2, num):
            #if divisible, break the loop
            if num % i == 0:
                break
        else:
            #otherwise add into prime list as num is not divisible by any num in the range
            prime_list.append(num)

print(prime_list)

#Using list comprehension
#Need to use 'all' function to test all possible divisions
pr_lt = [num for num in range(lo, hi + 1) if num > 1 and all(num % i != 0 for i in range(2,num))]

#print the obtained list
print(pr_lt)
