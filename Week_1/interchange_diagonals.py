#Program to accept a matrix of order MxM and interchange the diagonals.

#Function to interchange diagonals and print the matrix
def interchange_diagonals(mat, r):
    #Iterate through the range of matrix
    for i in range(r):
        temp = mat[i][i]
        mat[i][i] = mat[i][r - i - 1]
        mat[i][r - i - 1] = temp

    #Print the matrix
    for i in range(r):
        for j in range(r):
            #Print row elements on the same line
            print(mat[i][j], end=' ')
        #Print new line
        print()

#3 x 3 Matrix example
mat1 = [[2, 4, 5],
        [6, 6, 8],
        [3, 4, 1]]

#4 x 4 Matrix example
mat2 = [[3, 2, 4, 5],
        [9, 6, 6, 8],
        [5, 3, 4, 1],
        [1, 6, 2, 8]]

#Testing the code
print("Modified matrix 1:")
interchange_diagonals(mat1, 3)
print("Modified matrix 2:")
interchange_diagonals(mat2, 4)
