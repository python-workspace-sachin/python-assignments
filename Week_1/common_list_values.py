#Write a program to find common values from two lists

#Function to find the common values from 2 lists
def find_common(list1, list2):
    #Error Handling, check if any of the lists is empty
    if (list1 == [] or list2 == []):
        return "Invalid inputs"
    
    #Let's use set type of list1 so that it will have unique data
    #And intersection() method can be used to get a set of common values
    #Let's return the list type of obtaied set
    return list(set(list1).intersection(list2))

#Define 2 lists to test the code
l1 = [2,4,1,3,5,7,8,9]
l2 = [2,6,7,3,5]

#print returned answer on console
print(find_common(l1,l2))
