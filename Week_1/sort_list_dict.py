#Write a program to sort the list, dictionary.

print("\nList sorting")
#Example list
lt = [6,5,2,4,3,1,8,9]

#List sorting using built-in function
#Sort list in ascending order. By default, reverse=False
#Similarly, we have sorted() function which can be used if we don't want to perform in-place sorting

print("Sorting List in ascending order:")
lt.sort()
print(lt)

#Sort list in descending order
print("Sorting List in descending order:")
lt.sort(reverse=True)
print(lt)

lt = [6,5,2,4,3,1,8,9] # restore original list for next method
#Using Bubble sort technique
#Store the length of list in a variable
lt_size = len(lt)

#Use greater than operator to sort in ascending order
for i in range(0, lt_size - 1):
    for j in range(i + 1, lt_size):
        if lt[i] > lt[j]:
            #Logic to swap contents
            temp = lt[i]
            lt[i] = lt[j]
            lt[j] = temp

print("Sorting List in ascending order using bubble sort:")
print(lt)

#Use lesser than operator to sort in descending order
for i in range(0, lt_size - 1):
    for j in range(i + 1, lt_size):
        if lt[i] < lt[j]:
            #Logic to swap contents
            temp = lt[i]
            lt[i] = lt[j]
            lt[j] = temp

print("Sorting List in descending order using bubble sort:")
print(lt)

print("-----------------------------------------------------------------")
print("Dictionary sorting")
#Example dictionary
dt = {"orange": 5, "apple": 3, "mango":10, "banana": 4}

#Let's use sorted() function to sort the dictionary in a compact comprehension style
#Syntax for sorted() function with default values: sorted(iterable, key=None, reverse=False)
#We will use items() method as iterable to get a list containing the tuple for each key-value pair
#key can be extracted based on dictionary key or dictionary value by using simple lambda expression
#dictionary key => item: item[0]
#dictionary value => item: item[1]
#reverse=True will be used to sort in descending order

#Sort and print dictionary in ascending order of keys
print("Sorting in ascending order of keys:")
print({k: v for k, v in sorted(dt.items(), key=lambda item: item[0])})

#Sort and print dictionary in ascending order of values
print("Sorting in ascending order of values:")
print({k: v for k, v in sorted(dt.items(), key=lambda item: item[1])})

#Sort and print dictionary in descending order of keys
print("Sorting in descending order of keys:")
print({k: v for k, v in sorted(dt.items(), key=lambda item: item[0], reverse=True)})

#Sort and print dictionary in descending order of values
print("Sorting in descending order of values:")
print({k: v for k, v in sorted(dt.items(), key=lambda item: item[1], reverse=True)})
print()
