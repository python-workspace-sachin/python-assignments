#Program to accept a text file as input and print the text in a new file
#with the first alphabet of each word capitalized.
#Also, print the number of white spaces in the new file.

#Prompt user to enter the filename
print("Enter the input filename:", end=' ')

#Input file string
input_file = ''
#Get user input
input_file = input(input_file)
#print(input_file)

#Let's use try-except blocks for exception handling
try:
    #Let's use with open syntax to use the input file and close it after reading
    #name the opened file as myfile for simplicity
    with open(input_file) as myfile:
        #Store the contents of file in a string
        contents = myfile.read()
        print("Successfully read the contents from " + input_file)
except:
    #Prompt an error message and exit
    print("Error while opening the file.\nMake sure the file exists in current directory")
    exit()

#Convert every first letter of contents into capital letter
#capitalize() method converts only 1st letter of string into capital letter
#To convert every letter into capital letter, let's use title() method
contents = contents.title()
#Another method is to extract all the words from string, use capitalize() method to capitalize every word
#contents = ' '.join([word.capitalize() for word in contents.split()


#Get count of white space characters in the string
white_space_count = 0

for letter in contents: 
    #isspace() method is used to check if a letter is white space or not
    if letter.isspace() == True:
        white_space_count += 1

print("Capitalized contents:\n" + contents)
print("No. of whitespaces: " + str(white_space_count))

#Now create a new file in write mode to write contents onto it
#Let's use 'w+' mode so that we can create a new file or overwrite onto an existing file
#Let's name it as new_file
#Let's use try-except blocks for exception handling
try:
    with open("new_file.txt", mode='w+') as new_file:
        new_file.write(contents)
        new_file.write("\nNo. of white space characters: " + str(white_space_count))
        print("Successfully written the capitalized contents and whitespace count onto new_file.txt")
except:
    print("Error in creating or opening new_file")
    exit()