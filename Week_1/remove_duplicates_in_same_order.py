#Write a program to remove duplicate elements from the given list with the original order.

def remove_duplicates(lt):
    #Error handling
    if (lt == [] or len(lt) < 2):
        return lt
    
    #Create an extra empty list which would fill numbers with single occurrence
    ex = []
    #Create an empty list to hold the indices of numbers to be removed
    ex_index = []

    #Iterate through the list with its index and respective content
    #So, let's use enumerate() function
    for index, num in enumerate(lt):
        #append the num into ex if not already present
        #Otherwise append the respective index into ex_index
        ex.append(num) if num not in ex else ex_index.append(index)
    
    #Iterate through ex_index with its index and respective content
    #So, let's use enumerate() function again
    for i, ind in enumerate(ex_index):
        #After popping every element, size of lt reduces by 1
        #So, decrement ind by respective the i value to get appropriate index
        #and pop the item
        lt.pop(ind - i)

    #Now lt is modified. Return lt
    return lt

l1 = [2,4,6,2,4,5,8,3,6,9,3,3,5]

print(remove_duplicates(l1))
