#Write a program to convert decimal number into octal and vice versa.

#Function to convert decimal num into octal num
def dec_to_oct(num):
    #String to store reversed octal num
    rev_oct = ''
    while (num != 0):
        #Attach reminder in string form
        rev_oct += str(num % 8)
        #Divide num by 8
        num = int(num / 8)

    #Return intger form of reversed string
    return int(rev_oct[::-1])
 
# Function to convert octal num to decimal num
def oct_to_dec(num):
    #Ex = 45 => 4 * 8**1 + 5 * 8**0 => 37
    #Define a dec variable initialized to 0
    dec = 0
    #Define index value i as 0
    i = 0
    #Extract the last digit one by one
    while (num != 0):
        #Update dec with 
        dec += (num % 10) * (8**i)
        #Update num by dividing it 10
        num = int(num / 10)
        #increment i
        i += 1
        #Return dec value
    return dec

#Testing the code
print(dec_to_oct(37))
print(oct_to_dec(45))
