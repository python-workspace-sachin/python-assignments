#Write a method to take a list as an input and gives the output as a list of tuples. where each tuple will contain (index, value).
#example: input = [2,3,4,5,6]
#output = [(0,2), (1,3), (2,4), (3, 5), (4, 6)]

#Function to return the list of index-value tuple pairs
def index_value_tuple(lt):
    #Define an empty output list
    output = []
    #Let's use enumerate function to obtain both index and value from the given list 
    for index, value in enumerate(lt):
        #Append the tuple pair to output list 
        output.append((index, value))
    #Return output list
    return output

#Testing the code
print(index_value_tuple([2,3,4,5,6]))