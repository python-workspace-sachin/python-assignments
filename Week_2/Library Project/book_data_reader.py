import pickle

class Book():
   
    pass

pickle_in = open("book_data.pickle", mode='rb')
bookList = pickle.load(pickle_in)
pickle_in.close()

print("Sr No.\tBook Name\t\tAuthor Name\t\tISBN\t\tNo. of copies\tBorrowed by")
for index, book in enumerate(bookList):
    print("-----------------------------------------------------------------------------------------------------------------------------")
    print(f"{index + 1}.\t{book.bookName: <20}\t{book.authorName: <20}\t{book.ISBN}\t\t{book.noOfCopies}\t", end=' ')
    borrowerListLen = len(book.borrowersIdList)
    if borrowerListLen != 0:
        for index, userId in enumerate(book.borrowersIdList):
            print(f"{userId}", end="")
            print(",", end=" ") if (index != borrowerListLen - 1) else print()
    else:
        print("None")
print("-----------------------------------------------------------------------------------------------------------------------------")