import pickle

from random import randint
randint(100, 999)  # randint is inclusive at both ends

#Generic function to check if the given string contains numbers
def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

#Generic function to input user id and password
def get_id_and_password():
    while (True):
        try:
            userId = input("Can you please enter the User ID?: ")
            if (userId == ''):
                print("User Id cannot be an empty string!")
                continue
            break
        except:
            print("Invalid input! An appropriate string expected!")
    while (True):
        try:
            password = input("Can you please enter the Password?: ")
            if (password == ''):
                print("User Id cannot be an empty string!")
                continue
            break
        except:
            print("Invalid input! An appropriate string expected!")
    return userId, password

def delete_book_from_borrowers_list(bookName):
    try:
        with open("user_data.pickle", mode="rb") as pickle_in:
            user_list = pickle.load(pickle_in)
        for index, user in enumerate(user_list):
            if bookName in user.bookNamesBorrowed:
                user_list[index].noOfbooksBorrowed -= 1
                user_list[index].bookNamesBorrowed.remove(bookName)
                try:
                    with open("user_data.pickle", mode="wb+") as pickle_out:
                        pickle.dump(user_list, pickle_out)
                except:
                    print("Error in opening/overwriting onto user_data.pickle")
    except:
        print("Error in opening/reading from user_data.pickle")

#A Book class to handle information about every book
class Book():

    def __init__(self):
        self.bookName = ""
        self.authorName = ""
        self.noOfCopies = 0
        self.ISBN = randint(10**(12), (10**13)-1) #13 digit random number
        self.borrowersIdList = []

    def getBookInfo(self, bookName):
        self.bookName = bookName
        while (True):
            try:
                self.authorName = input("Enter name of the author: ")
                if (self.authorName == '' or hasNumbers(self.authorName)):
                    print("Invalid name string! Author name cannot be blank string and can only have alphabets!")
                    continue
                break
            except:
                print("An appropriate name string expected!")
        while (True):
            try:
                self.noOfCopies = int(input("Enter no. of copies: "))
                break
            except:
                print("An appropriate value expected!")

#User class to hold each user's data
class User():

    def __init__(self, userId, password):
        self.userId = userId
        self.password = password
        self.userName = ""
        self.noOfbooksBorrowed = 0
        self.bookNamesBorrowed = []

    def enterUserData(self):
        while (True):
            try:
                self.userName = input("Please enter your name: ")
                if (hasNumbers(self.userName)):
                    print("Invalid name string! User name can only have alphabets")
                    continue
                break
            except:
                print("Invalid input! An appropriate name string expected!")

    def update_data_in_file(self):
        try:
            with open("user_data.pickle", mode="rb") as pickle_in:
                userList = pickle.load(pickle_in)
            for index, user_item in enumerate(userList):
                if user_item.userId == self.userId:
                    userList[index].noOfbooksBorrowed = self.noOfbooksBorrowed
                    userList[index].bookNamesBorrowed = self.bookNamesBorrowed
                    #Overwrite new list data onto the file
                    try:
                        with open("user_data.pickle", mode="wb+") as pickle_out:
                            pickle.dump(userList, pickle_out)
                    except:
                        print("Error in opening/overwriting onto user_data.pickle")
                    return
            print("Could not find this user in user_data.pickle")
        except:
            print("Error in opening/reading from user_data.pickle")

    def display_user_menu(self):
        print("Welcome {}".format(self.userName.split()[0]))
        print("-----Library Menu-----\
            \n1. Display Available Books\
            \n2. Request a book\
            \n3. Return a book")

    def requestBook(self):
        while (True):
            try:
                userInput = input("Please enter the name of book you want to borrow: ")
                return userInput
            except:
                print("An appropriate string expected!")

    def returnBook(self):
        while (True):
            try:
                userInput = input("Please enter the name of book you want to return: ")
                return userInput
            except:
                print("An appropriate string expected!")

    def getUserInfo(self):
        #Fetch the user list from user_data file
        try:
            with open("user_data.pickle", mode="rb") as pickle_in:
                user_list = pickle.load(pickle_in)
            for user_item in user_list:
                if user_item.userId == self.userId and user_item.password == self.password:
                    self.userName = user_item.userName
                    self.bookNamesBorrowed = user_item.bookNamesBorrowed
                    self.noOfbooksBorrowed = user_item.noOfbooksBorrowed
                    return
            print("Sorry, unable to find this user in the database!")
        except:
            print("Error in opening/reading from user_data.pickle")

#Admin is sub-class of User class.It inherits properties of User class
class Admin(User):
        
    def addBook(self, booksStored):
        book = Book()
        while (True):
            try:
                bookName = input("Please enter name of the book: ")
                if (bookName == ''):
                    print("Book name cannot be blank! An appropriate book name expected!")
                    continue
                break
            except:
                print("Invalid input! An appropriate string expected!")
        if (booksStored == False):
            #First book
            book.getBookInfo(bookName)
            try:
                with open("book_data.pickle", mode="wb+") as pickle_out:
                    pickle.dump([book], pickle_out)
            except:
                print("Error opening/writing onto book_data.pickle")
            print("Successfully stored copy/copies of the book '" + book.bookName + "' into the library!")
            return True
        with open("book_data.pickle", mode="rb") as pickle_in:
            bookList = pickle.load(pickle_in)
        #Let's check for existing book
        for index, book_item in enumerate(bookList):
            if (book_item.bookName == bookName):
                print("This book already exists in library!")
                print("Author name: {}".format(book_item.authorName))
                while (True):
                    try:
                        noOfCopies = int(input("How many copies of this book do you want to add?: "))
                        if (noOfCopies < 1):
                            print("A positive count value expected!")
                            continue
                        break
                    except:
                        print("Invalid input! An appropriate integer value expected")
                #Get book info
                bookList[index].noOfCopies += noOfCopies
                try:
                    with open("book_data.pickle", "wb+") as pickle_out:
                        pickle.dump(bookList, pickle_out)
                except:
                    print("Error in opening/overwriting onto book_data.pickle")
                print("Given copy/copies of the book '" + bookName + "' are added to library!")
                del book
                return True
        book.getBookInfo(bookName)
        bookList.append(book)
        try:
            with open("book_data.pickle", "wb+") as pickle_out:
                pickle.dump(bookList, pickle_out)
            print("Successfully stored a copy of the book '" + book.bookName + "' into the library!")
            return True
        except:
            print("Error in opening/overwriting onto book_data.pickle")
            return False

    def deleteBook(self, bookName):
        try:
            with open("book_data.pickle", mode="rb") as pickle_in:
                bookList = pickle.load(pickle_in)
            #Let's find the book in library
            for book in bookList:
                if (book.bookName == bookName):
                    delete_book_from_borrowers_list(bookName)
                    #Remove book from the list
                    bookList.remove(book)
                    #Update in file
                    try:
                        with open("book_data.pickle", "wb+") as pickle_out:
                            pickle.dump(bookList, pickle_out)
                        print("Successfully deleted all copies of the book '" + bookName + "' from the library!")
                        return len(bookList)
                    except:
                        print("Error opening/overwriting onto book_data.pickle")
                        return -1
            print("This book does not exist in the library! Unable to delete!")
        except:
            print("Error opening/reading from book_data.pickle")
        return -1
    
    def update_data_in_file(self):
        try:
            with open("admin_data.pickle", mode="wb+") as pickle_out:
                pickle.dump(self, pickle_out)
        except:
            print("Error in opening/reading from user_data.pickle")

    def display_admin_menu(self):
        print("4. Add a book\
            \n5. Delete a book\
            \n6. Display current users")

    def print_admin_data(self):
        print("Sr No.\tUser Name\t\t\tUser Id\t\tUser Password\tBook count\tBook Name/s")
        print("--------------------------------------------------------------------------------------------------------------------")
        print(f"1.\t{self.userName: <20}\t\t{self.userId: <10}\t{self.password: <10}\t{self.noOfbooksBorrowed}\t\t", end=' ')
        if self.noOfbooksBorrowed > 0:
            for index, bookName in enumerate(self.bookNamesBorrowed):
                print(f"{bookName}", end=' ')
                print(",", end=' ') if index != self.noOfbooksBorrowed - 1 else print()
        else:
            print("None")

    def displayUsers(self, usersStored):
        self.print_admin_data()
        if (usersStored == False):
            print("--------------------------------------------------------------------------------------------------------------------")
            return
        #Read from user_data.pickle file
        try:
            with open("user_data.pickle", mode='rb') as pickle_in:
                userList = pickle.load(pickle_in)
            for index, user in enumerate(userList):
                print("--------------------------------------------------------------------------------------------------------------------")
                print(f"{index + 2}.\t{user.userName: <20}\t\t{user.userId: <10}\t{user.password: <10}\t{user.noOfbooksBorrowed}\t\t", end=' ')
                if user.noOfbooksBorrowed > 0:
                    for index, bookName in enumerate(user.bookNamesBorrowed):
                        print(f"{bookName}", end='')
                        print(",", end=" ") if (index != user.noOfbooksBorrowed - 1) else print()
                else:
                    print("None")
            print("--------------------------------------------------------------------------------------------------------------------")
        except:
            print("Error in opening/reading from the file: user_data.pickle")
    
    def getUserInfo(self):
        #Fetch the user list from user_data file
        try:
            with open("admin_data.pickle", mode="rb") as pickle_in:
                admin = pickle.load(pickle_in)
            self.userName = admin.userName
            self.noOfbooksBorrowed = admin.noOfbooksBorrowed
            self.bookNamesBorrowed = admin.bookNamesBorrowed
        except:
            print("Error in opening/reading from admin_data.pickle")

#Library class to manage user data and book data
class Library():

    def __init__(self, adminName, adminId, adminPassword, booksStored, usersStored):
        self.adminName = adminName
        self.adminId = adminId
        self.adminPassword = adminPassword
        self.booksStored = booksStored
        self.usersStored = usersStored
    
    def isAdmin(self, userId, password):
        return userId == self.adminId and password == self.adminPassword

    def display_login_menu(self):
        print("------User Login------")
        print("1. Sign In\
            \n2. Sign Up\
            \n3. Exit")

    def displayBooks(self, isThisAdmin):
        if (self.booksStored == False):
            print("There are no books in the library")
            return
        print("There are following books in the library: ")
        #Read from book_data.pickle file
        try:
            with open("book_data.pickle", mode='rb') as pickle_in:
                bookList = pickle.load(pickle_in)
            if (isThisAdmin):
                print("Sr No.\tBook Name\t\tAuthor Name\t\tISBN\t\tCopies count\tBorrowed by")
            else:
                print("Sr No.\tBook Name\t\tAuthor Name\t\tISBN\t\t\tAvailability")
            for index, book in enumerate(bookList):
                print("-----------------------------------------------------------------------------------------------------------------------------")
                if (isThisAdmin):
                    print(f"{index + 1}.\t{book.bookName: <20}\t{book.authorName: <20}\t{book.ISBN}\t\t{book.noOfCopies}\t", end=' ')
                elif book.noOfCopies > 0:
                    print(f"{index + 1}.\t{book.bookName: <20}\t{book.authorName: <20}\t{book.ISBN}\t\tAvailable")
                else:
                    print(f"{index + 1}.\t{book.bookName: <20}\t{book.authorName: <20}\t{book.ISBN}\t\tNot available")
                if (isThisAdmin):
                    borrowerListLen = len(book.borrowersIdList)
                    if borrowerListLen != 0:
                        for index, userId in enumerate(book.borrowersIdList):
                            print(f"{userId}", end="")
                            print(",", end=" ") if (index != borrowerListLen - 1) else print()
                    else:
                        print("None")
            print("-----------------------------------------------------------------------------------------------------------------------------")
        except:
            print("Error in opening/reading from the file: book_data.pickle")

    def lendBook(self, demandedBook, user):
        if demandedBook in user.bookNamesBorrowed:
            print("Sorry, you already borrowed a copy of this book! Extra copy is not provided")
            return False
        try:
            with open("book_data.pickle", mode='rb') as pickle_in:
                bookList = pickle.load(pickle_in)
            for index, book in enumerate(bookList):
                if (demandedBook == book.bookName) and (book.noOfCopies > 0):
                    bookList[index].noOfCopies -= 1
                    bookList[index].borrowersIdList.append(user.userId)
                    user.noOfbooksBorrowed += 1
                    user.bookNamesBorrowed.append(demandedBook)
                    #Let's update the pickle file again
                    try:
                        with open("book_data.pickle", mode="wb+") as pickle_out:
                            pickle.dump(bookList, pickle_out)
                        print("Congrats, you have borrowed a copy of this book")
                        return True
                    except:
                        print("Error in opening/overwriting onto book_data.pickle")
                        return False
            print("Sorry, this book is not available in the library")
        except:
            print("Error in opening/reading from book_data.pickle")
        return False

    def receiveBook(self, returnedBook, user):
        if returnedBook not in user.bookNamesBorrowed:
            print("You did not borrow this book! You can't return this book")
            return False
        #Read from book_data.pickle file
        try:
            with open("book_data.pickle", "rb") as pickle_in:
                bookList = pickle.load(pickle_in)
            #Find book in file
            for index, book in enumerate(bookList):
                if book.bookName == returnedBook:
                    #Increment the no. of copies
                    bookList[index].noOfCopies += 1
                    bookList[index].borrowersIdList.remove(user.userId)
                    user.noOfbooksBorrowed -= 1
                    user.bookNamesBorrowed.remove(returnedBook)
                    try:
                        with open("book_data.pickle", "wb+") as pickle_out:
                            pickle.dump(bookList, pickle_out)
                        print("Thanks for returning the book!")
                        return True
                    except:
                        print("Error in opening/reading from the file: book_data.pickle")
                        return False
        except:
            print("Error in opening/reading from the file: book_data.pickle")
        return False

def set_id_and_password(usersStored, userId):
    while (True):
        try:
            password = input("Can you please set a password?: ")
            if (password == ''):
                print("Password cannot be an empty string!")
                continue
            try:
                password_reentry = input("Can you please re-enter the password?: ")
                if (password == password_reentry):
                    print("Password entries matched")
                    #Let's create new user
                    user = User(userId, password)
                    user.enterUserData()
                    if (usersStored):
                        #Not first user
                        try:
                            with open("user_data.pickle", mode="rb") as pickle_in:
                                user_list = pickle.load(pickle_in)
                            user_list.append(user)
                            #Update user_data.pickle file
                            try:
                                with open("user_data.pickle", mode="wb+") as pickle_out:
                                    pickle.dump(user_list, pickle_out)
                            except:
                                print("Error while opening/overwriting onto user_data.pickle")
                                del user
                                return None
                        except:
                            print("Error while opening/reading from user_data.pickle")
                            del user
                            return None
                    else:
                        #First user entry
                        try:
                            with open("user_data.pickle", mode="wb+") as pickle_out:
                                pickle.dump([user], pickle_out)
                        except:
                            print("Error while opening/writing onto user_data.pickle")
                            del user
                            return None
                    print("Successfully created new account!")
                    return user
                else:
                    print("Password entries unmatched! You need to set the password again!")
            except:
                print("Invalid input! An appropriate string expected")
        except:
            print("Invalid input! An appropriate string expected")

def verify_id_and_password(userId, usersStored, password):
    if (usersStored == False):
        print("Not a registered user. Please login as admin or select 'Sign Up' option.")
        return False
    #Fetch the user list from user_data file
    try:
        with open("user_data.pickle", mode="rb") as pickle_in:
            user_list = pickle.load(pickle_in)
        for user in user_list:
            if user.userId == userId and user.password == password:
                return True
        print("Incorrect user Id or password! Try again later")
        return False
    except:
        print("Error in opening/reading from user_data.pickle")

def userMenu(user, library):
    while (True):
        user.display_user_menu()
        print("4. Sign Out")
        try:
            choice = int(input("Please enter a choice: "))
            if (choice == 1):
                library.displayBooks(False)
            elif (choice == 2):
                if (library.booksStored == False):
                    print("Sorry, there are no books in the library! Only admin can add books!")
                elif (library.lendBook(user.requestBook(), user)):
                    user.update_data_in_file()
            elif (choice == 3):
                if (library.booksStored == False):
                    print("Sorry, there are no books in the library! Only admin can add books!")
                elif user.noOfbooksBorrowed == 0:
                    print("You have not borrowed any book from library")
                elif (library.receiveBook(user.returnBook(), user)):
                    user.update_data_in_file()
            elif (choice == 4):
                print("Thanks. You are successfully signed out!")
                #Delete user object from local memory
                del user
                break
            else:
                print("Invalid choice! An appropriate choice number expected!")
        except:
            print("Invalid choice! An appropriate choice number expected")

def main():
    try:
        with open("user_data.pickle", mode="rb") as pickle_in:
            userList = pickle.load(pickle_in)
    except:
        userList = []
    usersStored = False if userList == [] else True
    try:
        with open("book_data.pickle", mode="rb") as pickle_in:
            bookList = pickle.load(pickle_in)
    except:
        bookList = []
    booksStored = False if bookList == [] else True
    del bookList, userList
    library = Library("Admin", "admin", "adminpswd", booksStored, usersStored)
    try:
        with open("admin_data.pickle", mode="rb") as pickle_in:
            admin = pickle.load(pickle_in)
    except:
        admin = None
    if admin == None:
        admin = Admin(library.adminId, library.adminPassword)
        admin.userName = library.adminName
        try:
            with open("admin_data.pickle", mode="wb+") as pickle_out:
                pickle.dump(admin, pickle_out)
            del admin
        except:
            print("Error in opening/writing onto admin_data.pickle")
    print("----------------------")
    print("Welcome to the Library")
    print("----------------------")
    while (True):
        library.display_login_menu()
        try:
            option = int(input("Please select an option: "))
            if (option == 1):
                userId, password = get_id_and_password()
                if (library.isAdmin(userId, password)):
                    admin = Admin(library.adminId, library.adminPassword)
                    admin.getUserInfo()
                    while (True):
                        admin.display_user_menu()
                        admin.display_admin_menu()
                        print("7. Sign Out")
                        try:
                            choice = int(input("Please enter a choice: "))
                            if (choice == 1):
                                library.displayBooks(True)
                            elif (choice == 2):
                                if (library.booksStored == False):
                                    print("Sorry, there are no books in the library! Only admin can add books!")
                                elif (library.lendBook(admin.requestBook(), admin)):
                                    admin.update_data_in_file()
                            elif (choice == 3):
                                if (library.booksStored == False):
                                    print("Sorry, there are no books in the library! Only admin can add books!")
                                elif admin.noOfbooksBorrowed == 0:
                                    print("You have not borrowed any book from library")
                                elif (library.receiveBook(admin.returnBook(), admin)):
                                    admin.update_data_in_file()
                            elif (choice == 4):
                                admin.addBook(library.booksStored)
                                library.booksStored = True
                            elif (choice == 5):
                                if (library.booksStored == False):
                                    print("There are no books in the library! Only Admin can add new books!")
                                else:
                                    while (True):
                                        try:
                                            bookName = input("Please enter the name of the book which you want to delete: ")
                                            if (bookName == ''):
                                                print("Invalid input! Book name cannot be blank string!")
                                                continue
                                            if (admin.deleteBook(bookName) == 0):
                                                library.booksStored = False
                                            break
                                        except:
                                            print("Invalid input! An appropriate name string expected!")
                            elif (choice == 6):
                                admin.displayUsers(library.usersStored)
                            elif (choice == 7):
                                print("Thanks. You are successfully signed out!")
                                del admin
                                break
                            else:
                                print("Invalid choice! An appropriate choice number expected!")
                        except:
                            print("Invalid input! An appropriate chice number expected!")
                elif (verify_id_and_password(userId, library.usersStored, password)):
                    user = User(userId, password)
                    user.getUserInfo()
                    userMenu(user, library)
            elif (option == 2):
                while (True):
                    try:
                        userId = input("Can you please set an user Id?: ")
                        if (userId == ''):
                            print("User Id cannot be an empty string")
                        else:
                            break
                    except:
                        print("Invalid input! An appropriate string expected!")
                if (library.usersStored):
                    #Check if this is existing user
                    try:
                        with open("user_data.pickle", mode="rb") as pickle_in:
                            user_list = pickle.load(pickle_in)
                        existingUser = False
                        for user_item in user_list:
                            if (user_item.userId == userId):
                                #This is existing user
                                print("This user Id already exists! Please select 'Sign In' option.")
                                existingUser = True
                                break
                        if (existingUser):
                            continue
                    except:
                        print("Error opening/reading from user_data.pickle")
                        continue
                user = set_id_and_password(library.usersStored, userId)
                if (user != None):
                    library.usersStored = True
                    userMenu(user, library)
            elif (option == 3):
                print("Thanks for visiting.\nSee you soon")
                break
            else:
                print("Invalid option! An appropriate option number expected!")
        except:
            print("Invalid input! An appropriate option number expected!")

main()
