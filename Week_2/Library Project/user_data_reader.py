import pickle

class User():
   
   pass

class Admin(User):

   pass

pickle_in = open("admin_data.pickle", mode='rb')
admin = pickle.load(pickle_in)
pickle_in.close()

pickle_in = open("user_data.pickle", mode='rb')
userList = pickle.load(pickle_in)
pickle_in.close()

print("Sr No.\tUser Name\t\t\tUser Id\t\tUser Password\tBook count\tBook Name/s")
print("--------------------------------------------------------------------------------------------------------------------")
print(f"1.\t{admin.userName: <20}\t\t{admin.userId: <10}\t{admin.password: <10}\t{admin.noOfbooksBorrowed}\t\t", end=' ')
adminBooksLen = len(admin.bookNamesBorrowed)
if admin.noOfbooksBorrowed > 0:
    for index, bookName in enumerate(admin.bookNamesBorrowed):
        print(f"{bookName}", end='')
        print(",", end=' ') if index != adminBooksLen - 1 else print()
else:
    print("None")
for index, user in enumerate(userList):
    print("--------------------------------------------------------------------------------------------------------------------")
    print(f"{index + 2}.\t{user.userName: <20}\t\t{user.userId: <10}\t{user.password: <10}\t{user.noOfbooksBorrowed}\t\t", end=' ')
    bookNameListLen = len(user.bookNamesBorrowed)
    if user.noOfbooksBorrowed > 0:
        for index, bookName in enumerate(user.bookNamesBorrowed):
            print(f"{bookName}", end='')
            print(",", end=" ") if (index != bookNameListLen - 1) else print()
    else:
        print("None")
print("--------------------------------------------------------------------------------------------------------------------")