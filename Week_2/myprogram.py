#To import a function from a module
#from mymodule import my_func
#my_func()

#To import a module from package
from AddSubtract import add
from AddSubtract import subtract
#To import a module from subpackage 
from AddSubtract.MultiplyDivide import multiply
from AddSubtract.MultiplyDivide import divide

num1 = int(input("Enter num1: "))
num2 = int(input("Enter num2: "))

print(f"{num1} + {num2} = {add.add(num1, num2)}")
print(f"{num1} - {num2} = {subtract.subtract(num1, num2)}")
print(f"{num1} x {num2} = {multiply.multiply(num1, num2)}")
print(f"{num1} / {num2} = {divide.divide(num1, num2)}")
