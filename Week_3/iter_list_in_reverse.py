#Write a program that takes a list and iterates it from the reverse direction. (Write iterator class for this)

class RevIterator():
    def __init__(self, list_item):
        self.list_item = list_item
        self.list_index = len(list_item)

    def __iter__(self):
        return self

    def __next__(self):
        self.list_index -= 1
        if self.list_index < 0:
            raise StopIteration
        else:
            return self.list_item[self.list_index]

lst = RevIterator([1, 10, 5, 6, 4, 3, 7])

print(list(lst))