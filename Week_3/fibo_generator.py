#Write a program to generate Fibonacci numbers using a generator.

def generate_fibonacci(limit):
    
    #Error handling
    if (limit < 0):
        yield "Invalid Input"
    #Variables holding previous and current value

    prev = 0 #First num in the series
    current = 1 #Second num in the series

    #Yield every prev value from the loop till given limit
    while (prev <= limit):
        yield prev
        #Update prev and current values
        prev, current = current, prev + current

print("Fibonacci sequence: ", end=' ')
for num in generate_fibonacci(80):
    print(num, end=" ")

print()