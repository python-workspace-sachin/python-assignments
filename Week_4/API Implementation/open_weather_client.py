import http.client

conn = http.client.HTTPSConnection("community-open-weather-map.p.rapidapi.com")

headers = {
    'x-rapidapi-key': "d8636c780emsh090e6037b287e50p1a6d50jsnd73d12de864e",
    'x-rapidapi-host': "community-open-weather-map.p.rapidapi.com"
    }

conn.request("GET", "/weather?q=Bengaluru%2C%20In&lat=0&lon=0&callback=test&id=2172797&lang=null&units=%22metric%22%20or%20%22imperial%22&mode=xml%2C%20html", headers=headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))