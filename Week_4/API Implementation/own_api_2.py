from flask import Flask, request, jsonify
from flask_restful import Resource, Api, reqparse
import pandas as pd
import mysql.connector
from mysql.connector import errorcode
import ast

config = {
    'user': 'root',
    'password': 'mysqlsachdb!16',
    'host': 'localhost',
    'database': 'cafe'
}

db = mysql.connector.connect(**config)
cursor = db.cursor()

DB_NAME = 'cafe'

TABLES = {}

user_keys = ('id', 'user_id', 'name', 'city', 'time_stamp', 'location')

TABLES['users'] = (
    "CREATE TABLE `users` ("
    " `id` int(11) NOT NULL AUTO_INCREMENT,"
    " `user_id` varchar(25) NOT NULL,"
    " `name` varchar(25) NOT NULL,"
    "`city` varchar(20) NOT NULL,"
    " `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,"
    "`location` varchar(100) NOT NULL,"
    " PRIMARY KEY (`id`)"
    ") ENGINE=InnoDB"
)

TABLES['location'] = (
    "CREATE TABLE `location` ("
    " `location_id` int(11) NOT NULL AUTO_INCREMENT,"
    " `name` varchar(25) NOT NULL,"
    "`rating` varchar(10) NOT NULL,"
    "PRIMARY KEY (`location_id`),"
    "FOREIGN KEY (`location_id`) REFERENCES users (`id`)"
    ") ENGINE=InnoDB"
)

def create_database():
    cursor.execute(
        "CREATE DATABASE IF NOT EXISTS {} DEFAULT CHARACTER SET 'utf8'".format(DB_NAME))
    print("Database {} created!".format(DB_NAME))

def create_tables():
    cursor.execute("USE {}".format(DB_NAME))

    for table_name in TABLES:
        table_description = TABLES[table_name]
        try:
            print("Creating table ({})".format(table_name))
            cursor.execute(table_description)
            print("Table of {} is created".format(table_name))
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("Already Exists")
            else:
                print(err.msg)

#create_database()
create_tables()

app = Flask(__name__)
api = Api(app)

def get_json_list(tup_list):
        json_list = []
        for tup in tup_list:
            json_list.append(dict(zip(user_keys, tup)))
        return json_list

class Users(Resource):
    def get(self):
        sql = ("SELECT * FROM users")
        cursor.execute(sql)
        result = get_json_list(cursor.fetchall()) 
        if result != []:
            return jsonify(result), 200  # return data and 200 OK
        else:
            return {
                'message': "Users table is empty!"
            }, 400

    def post(self):
        parser = reqparse.RequestParser()  # initialize
        parser.add_argument('user_id', required=True)  # add args
        parser.add_argument('name', required=True)
        parser.add_argument('city', required=True)
        parser.add_argument('location', required=False)
        args = parser.parse_args()  # parse arguments to dictionary
        
        # read our SQL
        sql = ("SELECT user_id FROM users")
        cursor.execute(sql)
        result = get_json_list(cursor.fetchall())

        if result != []:
            for each in result:
                if args['user_id'] in each:
                    return {
                        'message': f"'{args['user_id']}' already exists."
                    }, 409
        else:
            # create new dataframe containing new values
            sql = ("INSERT INTO logs(user_id, name, city) VALUES (%s, %s, %s)")
            cursor.execute(sql, (args['name'], args['city']))
            db.commit()
            cursor.execute(sql)
            print("Added data for {}".format(args['user_id']))
            # add the newly provided values
            result = get_json_list(cursor.fetchall())
            return jsonify(result), 200  # return data with 200 OK

    def put(self):
        parser = reqparse.RequestParser()  # initialize
        parser.add_argument('user_id', required=True)  # add args
        parser.add_argument('location', required=True)
        args = parser.parse_args()  # parse arguments to dictionary

        # read our SQL
        sql = ("SELECT user_id FROM users")
        cursor.execute(sql)
        result = get_json_list(cursor.fetchall())
        
        if result != []:
            for each in result:
                if args['user_id'] in each:
                    each['location'].append(args['location'])
            
                    # save back to DB
                    sql = ("UPDATE logs SET location = %s WHERE user_id = %s")
                    cursor.execute(sql, (args['location'], args['user_id']))
                    db.commit()
                    print("User data updated")
                    # return data and 200 OK
                    return jsonify(result), 200
        else:
            # otherwise the userId does not exist
            return {
                'message': f"'{args['user_id']}' user not found."
            }, 404

    def delete(self):
        parser = reqparse.RequestParser()  # initialize
        parser.add_argument('user_id', required=True)  # add userId arg
        args = parser.parse_args()  # parse arguments to dictionary
        
        # read our SQL
        sql = ("SELECT user_id FROM users")
        cursor.execute(sql)
        result = get_json_list(cursor.fetchall())
        
        if result != []:
            for each in result:
                if args['user_id'] in each:
                    # remove data entry matching given userId
                    result.remove(each)

                    # save back to SQL
                    sql = ("DELETE FROM logs WHERE user_id = %s")
                    cursor.execute(sql, (args['user_id']))
                    db.commit()
                    print("User removed")
                    # return data and 200 OK
                    return jsonify(result), 200
        else:
            # otherwise we return 404 because userId does not exist
            return {
                'message': "User Id not found."
            }, 404

                    
class Locations(Resource):
    def get(self):
        sql = ("SELECT * FROM locations")
        cursor.execute(sql)
        result = get_json_list(cursor.fetchall())
        return jsonify(result), 200  # return data and 200 OK
    
    def post(self):
        parser = reqparse.RequestParser()  # initialize parser
        parser.add_argument('location_id', required=True, type=int)  # add args
        parser.add_argument('name', required=True)
        parser.add_argument('rating', required=True)
        args = parser.parse_args()  # parse arguments to dictionary
        
        # read our SQL
        sql = ("SELECT user_id FROM locations")
        cursor.execute(sql)
        result = get_json_list(cursor.fetchall())
        
        if result != []:
            # check if location already exists
            for each in result:
                if args['location_id'] in each:
                    return {
                        'message': f"'{args['location_id']}' already exists."
                    }, 409
        else:
            # otherwise, we can add the new location record
            sql = ("INSERT INTO locations(location_id, name, rating) VALUES (%s, %s, %s)")
            cursor.execute(sql, (args['location_id'], args['name'], args['rating']))
            db.commit()
            print("Location updated")
            result = get_json_list(cursor.fetchall())
            return jsonify(result), 200  # return data with 200 OK
    
    def delete(self):
        parser = reqparse.RequestParser()  # initialize parser
        parser.add_argument('location_id', required=True, type=int)  # add location_id arg
        args = parser.parse_args()  # parse arguments to dictionary

        # read our SQL
        sql = ("SELECT user_id FROM users")
        cursor.execute(sql)
        result = cursor.fetchall()
        
        if result != []:
            # check that the locationId exists
            for each in result:
                if args['user_id'] in each:
                    # remove data entry matching given userId
                    result.remove(each)
                    # save back to SQL
                    sql = ("DELETE FROM logs WHERE user_id = %s")
                    cursor.execute(sql, (args['location_id']))
                    db.commit()
                    print("User removed")
                    # return data and 200 OK
                    return jsonify(result), 200
        else:
            # otherwise we return 404 not found
            return {
                'message': f"Location Id '{args['location_id']}' does not exist."
            }


api.add_resource(Users, '/users')  # add endpoints
api.add_resource(Locations, '/locations')

if __name__ == '__main__':
    app.run()  # run our Flask app